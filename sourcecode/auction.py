
import os
import sqlite3
import random
import bcrypt
from functools import wraps

from logging.handlers import RotatingFileHandler
from flask import Flask, render_template, url_for, abort, request, g, flash, redirect, session

app = Flask(__name__)

db_location = 'var/shop.db'
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'


###################################################################
#
#                           User Login
#
###################################################################


def check_auth(email, password):
    if(email == valid_email and
        valid_pwhash == bcrypt.hashpw(password.encode('utf-8'), valid_pwhash)):
            return True
    return False

def requires_login(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        status = session.get('logged_in', False)
        if not status:
            return redirect(url_for('.index'))
        return f(*args, **kwargs)
    return decorated

@app.route('/logout/')
def logout():
    session['logged_in'] = False
    session.pop('username', None)
    return redirect(url_for('.index'))

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        db = get_db()
        cursor = db.cursor()

        sql = 'SELECT email, password, username FROM users WHERE email = ?'
        rows_count = cursor.execute(sql, (email,))
        retrieved = cursor.fetchone()
        #return password
        if retrieved is None:
           flash('User not found')
           return render_template('userlogin.html')
        else:
           hashed = str(retrieved[1])
           if hashed == bcrypt.hashpw(password.encode('utf-8'), hashed):
                #return password
                username = str(retrieved[2])
                session['logged_in'] = True
                session['username'] = username
                return redirect(url_for('account', userID=username))
    return render_template('userlogin.html')

###################################################################
#
#                           Functions
#
###################################################################

def generateID():
    id = random.randint(1, 100000)
    return str(id)
###################################################################
#
#                           Database
#
###################################################################
def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = sqlite3.connect(db_location)
        g.db = db
    return db

@app.teardown_appcontext
def close_db_connection(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('static/schema/database.sql', mode='r') as f:
         db.cursor().executescript(f.read())
        db.commit()

###################################################################
#
#                           ROUTES
#
###################################################################

@app.route('/listing/<item>/')
def listing(item):
 if item == '':
  return redirect(url_for('index'))
 else:
     db = get_db()
     cursor = db.cursor()

     sql = 'SELECT * FROM listing WHERE listingID = ?'
     rows_count = cursor.execute(sql, (item,))
     retrieved = cursor.fetchone()
     if retrieved is None:
        not_found = 'No listing found for ' + str(item)
        return render_template('listing.html', item=not_found)
     else:
         listingID = str(retrieved[0])
         seller = str(retrieved[1])
         title = str(retrieved[2])
         price = str(retrieved[3])
         condition = str(retrieved[4])
         size = str(retrieved[5])
         description = str(retrieved[6])
         db.close()

         editable = session.get('username', None)
         if editable == seller:
            return redirect(url_for('edit_listing', listingID = listingID))
            #return render_template('listing.html', id=listingID, seller=seller, title=title, price=price, condition=condition, size=size, description=description)
         else:

            return render_template('listing.html', id=listingID, seller=seller, title=title, price=price, condition=condition, size=size, description=description)


@app.route('/')
def index():
    images = os.listdir(os.path.join(app.static_folder, "images"))
    return render_template('trending.html', images=images)

@app.route('/edit/<listingID>/' , methods=['POST','GET'])
@requires_login
def edit_listing(listingID=None):

    if request.method == 'POST':
        db = get_db()
        cursor = db.cursor()

        title = request.form['title']
        price = request.form['price']
        size = request.form['size']
        description = request.form['description']

        sql = 'UPDATE listing SET title=?, price=?, size=?, description=? WHERE listingID =?'

        db.cursor().execute(sql, (title, price, size, description, listingID))
        db.commit()

        return redirect(url_for('account'))
    else:
        db = get_db()
        cursor = db.cursor()

        sql = 'SELECT * FROM listing WHERE listingID = ?'
        rows_count = cursor.execute(sql, (listingID,))
        retrieved = cursor.fetchone()

        listingID = str(retrieved[0])
        seller = str(retrieved[1])
        title = str(retrieved[2])
        price = str(retrieved[3])
        condition = str(retrieved[4])
        size = str(retrieved[5])
        description = str(retrieved[6])
        db.close()

        return render_template('edit.html', listingID=listingID, seller=seller, title=title, price=price, condition=condition, size=size, description=description)


@app.route('/signup/', methods=['POST','GET'])
def signup():
    if request.method == 'POST':
        db = get_db()
        print request.form
        username = request.form['username']
        password = bcrypt.hashpw((request.form['password']).encode('utf-8'), bcrypt.gensalt())
        email = request.form['email']
        forename = request.form['forename']
        surname = request.form['surname']

        db.cursor().execute("INSERT INTO users(username, password, email, forename, surname) VALUES (?,?,?,?,?)", (username, password, email, forename, surname))
        db.commit()
        return redirect(url_for('account'))
    else:
     return render_template('signup.html'), 200

@app.route('/account', methods=['POST','GET'])
@requires_login
def account():
    if request.method == 'POST':
        db = get_db()
        listingID = generateID()
        print request.form
        if 'username' in session:
            lister = session.get('username', None)
        title = request.form['title']
        price = request.form['price']
        condition = request.form['condition']
        size = request.form['size']
        description = request.form['description']

        f = request.files['itempicture']
        f.save('static/images/' + listingID +'.png' )

        filepath = f.filename

        db.cursor().execute("INSERT INTO listing(listingID, username, title, price, condition, size, description, imagepath) VALUES (?,?,?,?,?,?,?,?)", (listingID, lister, title, price, condition, size, description, filepath))
        db.commit()
        flash('You have listed an item')
        #return redirect(url_for('account'))
        return render_template("account.html", logged_in = lister)
    else:
       db = get_db()
       cursor = db.cursor()

       sql = 'SELECT * FROM listing WHERE username = ?'
       if 'username' in session:
           lister = session.get('username', None)
       rows_count = cursor.execute(sql, (lister,))
       retrieved = cursor.fetchall()
       if retrieved is None:
          not_found = 'No listings'
          return render_template('account.html', search=not_found)
       else:
           search_results = []
           for row in retrieved:
               search_results.append(row[0])

           db.close()
       return render_template("account.html", found=search_results, logged_in = lister)

@app.route('/user/<userID>/')
def userpage(userID=None):
    if userID == '':
     return redirect(url_for('index'))
    else:
        db = get_db()
        cursor = db.cursor()

        sql = 'SELECT * FROM listing WHERE username = ?'
        rows_count = cursor.execute(sql, (userID,))
        retrieved = cursor.fetchall()
        if retrieved is None:
           return redirect(url_for('index'))
        else:
           search_results = []
           for row in retrieved:
               search_results.append(row[0])

           db.close()
           return render_template('search_results.html', found=search_results)

@app.route('/search/<title>/')
def search(title=None):
    if title == '':
     return redirect(url_for('index'))
    else:
        db = get_db()
        cursor = db.cursor()

        sql = 'SELECT * FROM listing WHERE title = ?'
        rows_count = cursor.execute(sql, (title,))
        retrieved = cursor.fetchall()
        if retrieved is None:
           not_found = 'No search results'
           return render_template('listing.html', search=not_found)
        else:
           search_results = []
           for row in retrieved:
               search_results.append(row[0])

           db.close()
           return render_template('search_results.html', found=search_results)

###################################################################
#
#                           ERRORS
#
###################################################################
@app.errorhandler(404)
def page_not_found(error):
  return render_template('errors/404.html'), 404

@app.errorhandler(500)
def internal_server_error(error):
  return render_template('errors/500.html'), 500

@app.route('/force500')
def force500():
    abort(500)


if __name__ == "__main__":

    app.run(host='0.0.0.0', debug=True)
