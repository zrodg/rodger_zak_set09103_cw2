DROP TABLE if EXISTS users;
DROP TABLE if EXISTS listing;

CREATE TABLE users (
  username VARCHAR(15),
  password VARCHAR(255),
  email VARCHAR(255),
  forename VARCHAR(255),
  surname VARCHAR(255),
  PRIMARY KEY (username)
);

CREATE TABLE listing (
  listingID VARCHAR(30),
  username VARCHAR(15),
  title VARCHAR(30),
  price FLOAT(3,2),
  condition VARCHAR(255),
  size VARCHAR(15),
  description VARCHAR(255),
  imagepath VARCHAR(255),
  decrease VARCHAR(20),
  sold VARCHAR(5),
  FOREIGN KEY (username) REFERENCES users(username)
);
